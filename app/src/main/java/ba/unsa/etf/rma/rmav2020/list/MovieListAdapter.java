package ba.unsa.etf.rma.rmav2020.list;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;

import java.lang.reflect.Field;
import java.util.ArrayList;

import ba.unsa.etf.rma.rmav2020.R;
import ba.unsa.etf.rma.rmav2020.data.Movie;

public class MovieListAdapter extends ArrayAdapter<Movie> {

    private int resource;
    public TextView titleView;
    public TextView genreView;
    public ImageView imageView;
    private String posterPath="https://image.tmdb.org/t/p/w342";

    public MovieListAdapter(@NonNull Context context, int _resource, ArrayList<Movie> items) {
        super(context, _resource,items);
        resource = _resource;
    }



    public void setMovies(ArrayList<Movie> movies) {
        this.addAll(movies);
    }

    public Movie getMovie(int position) {return this.getItem(position);}

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LinearLayout newView;
        if (convertView == null) {
            newView = new LinearLayout(getContext());
            String inflater = Context.LAYOUT_INFLATER_SERVICE;
            LayoutInflater li;
            li = (LayoutInflater)getContext().
                    getSystemService(inflater);
            li.inflate(resource, newView, true);
        } else {
            newView = (LinearLayout)convertView;
        }



        Movie movie = getItem(position);
        titleView =  newView.findViewById(R.id.title);
        genreView =  newView.findViewById(R.id.genre);
        imageView =  newView.findViewById(R.id.icon);
        titleView.setText(movie.getTitle());
        genreView.setText(movie.getReleaseDate());
        String genreMatch = movie.getGenre();

        Glide.with(getContext())
                .load(posterPath+movie.getPosterPath())
                .centerCrop()
                .placeholder(R.drawable.picture1)
                .error(R.drawable.picture1)
                .fallback(R.drawable.picture1)
                .into(imageView);

        return newView;
    }
}
