package ba.unsa.etf.rma.rmav2020.list;

import android.content.Context;

import java.util.ArrayList;

import ba.unsa.etf.rma.rmav2020.data.Movie;

public class MovieListPresenter implements IMovieListPresenter, MovieListInteractor.OnMoviesSearchDone {
    private IMovieListView view;
    private IMovieListInteractor interactor;
    private Context context;

    public MovieListPresenter(IMovieListView view, Context context) {
        this.view       = view;
        this.interactor = new MovieListInteractor(this);
        this.context    = context;
    }


    @Override
    public void onDone(ArrayList<Movie> results) {
        view.setMovies(results);
        view.notifyMovieListDataSetChanged();
    }

    @Override
    public void searchMovies(String query){
        new MovieListInteractor((MovieListInteractor.OnMoviesSearchDone)
                this).execute(query);
    }

}
