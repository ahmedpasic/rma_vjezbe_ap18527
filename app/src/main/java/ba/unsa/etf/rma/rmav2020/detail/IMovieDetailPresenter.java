package ba.unsa.etf.rma.rmav2020.detail;

import android.os.Parcelable;

import java.util.ArrayList;

import ba.unsa.etf.rma.rmav2020.data.Movie;

public interface IMovieDetailPresenter {

    void create(int id, String posterPath, String title, String overview, String releaseDate, String genre, String homepage, ArrayList<String> actors);
    void setMovie(Parcelable movie);
    Movie getMovie();
}
