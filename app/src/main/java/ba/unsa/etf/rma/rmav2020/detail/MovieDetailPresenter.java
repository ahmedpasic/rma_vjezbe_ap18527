package ba.unsa.etf.rma.rmav2020.detail;

import android.content.Context;
import android.os.Parcelable;

import java.util.ArrayList;

import ba.unsa.etf.rma.rmav2020.data.Movie;

public class MovieDetailPresenter implements IMovieDetailPresenter {

    private Context context;

    private Movie movie;


    public MovieDetailPresenter(Context context) {
        this.context    = context;
    }

    @Override
    public void create(int id, String posterPath, String title, String overview, String releaseDate, String genre, String homepage, ArrayList<String> actors) {
        this.movie = new Movie(id, posterPath, title,overview,releaseDate,homepage,genre,actors);
    }

    @Override
    public void setMovie(Parcelable movie) {
        this.movie = (Movie)movie;
    }

    @Override
    public Movie getMovie() {
        return movie;
    }
}
